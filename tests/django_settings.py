import os
from typing import Any

BASE_DIR = os.path.dirname(__file__)
SETTINGS_MODULE = "tests.django_settings"

INSTALLED_APPS = ("bootstrap_pagination",)

DATABASES: dict[str, Any] = {}
MIDDLEWARE_CLASSES = ()

ROOT_URLCONF = __name__
SECRET_KEY = "secretkey"
SITE_ROOT = "."
USE_TZ = True

TEMPLATE_DEBUG = True
TEMPLATE_DIRS = ()

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": TEMPLATE_DIRS,
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [],
            "debug": TEMPLATE_DEBUG,
        },
    },
]
LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "verbose": {
            "format": "{levelname} {asctime} {module} {message}",
            "style": "{",
        },
    },
    "handlers": {
        "console": {
            "level": "DEBUG",
            "class": "logging.StreamHandler",
            "formatter": "verbose",
        },
        "logfile": {
            "level": "DEBUG",
            "class": "logging.handlers.RotatingFileHandler",
            "filename": BASE_DIR + "/logfile.log",
            "maxBytes": 50000,
            "backupCount": 2,
            "formatter": "verbose",
        },
    },
    "loggers": {
        "": {
            "handlers": ["console", "logfile"],
            "level": "DEBUG",
        },
        "django.utils.autoreload": {  # no need to tell us about every reloaded>
            "handlers": ["console"],
            "level": "INFO",
            "propagate": False,
        },
        "django.template": {  # no need to tell us about template lookup misses
            "handlers": ["console"],
            "level": "INFO",
            "propagate": False,
        },
    },
}

urlpatterns: list[Any] = []
