# Change Log

## [2.1.0](https://gitlab.com/nmeth/django-bootstrap-pagination-nigelm/compare/v2.0.1...v2.1.0) (2024-04-04)


### Features

* Attempt to add a url_base parameter ([3e52d64](https://gitlab.com/nmeth/django-bootstrap-pagination-nigelm/commit/3e52d64c77520d707b992fc96a7ef52836d820ff))


### Miscellaneous Chores

* update dependencies ([7f4b248](https://gitlab.com/nmeth/django-bootstrap-pagination-nigelm/commit/7f4b248420f547055290919572ec99e49552fc30))

## [2.0.1](https://gitlab.com/nmeth/django-bootstrap-pagination-nigelm/compare/v2.0.0...v2.0.1) (2023-11-22)


### Bug Fixes

* Correct tests for wrapped pager ([2f9254a](https://gitlab.com/nmeth/django-bootstrap-pagination-nigelm/commit/2f9254ab8b275de9be7ff7464ed24da9b085bec7))
* Wrap the pager in a nav element ([65cc614](https://gitlab.com/nmeth/django-bootstrap-pagination-nigelm/commit/65cc614fd86c6e8f9f8874bdf1a1b2465410b620))


### Miscellaneous Chores

* Fixed punctuation in .releaserc.yaml ([2664877](https://gitlab.com/nmeth/django-bootstrap-pagination-nigelm/commit/2664877c84ac95125ba0a4b4084c0221f1796007))


### CI/CD

* Added publish stage ([4bbefd8](https://gitlab.com/nmeth/django-bootstrap-pagination-nigelm/commit/4bbefd8cd28c605618796d209dda9388b1af783e))
* apparently the extension must be .yml ([befaf5c](https://gitlab.com/nmeth/django-bootstrap-pagination-nigelm/commit/befaf5cb626e2a476fc8c02756095192cde31fda))
* set up for CI and automated release ([34e151a](https://gitlab.com/nmeth/django-bootstrap-pagination-nigelm/commit/34e151a4c3a245c069009262dbf2ff876bf9f9af))
